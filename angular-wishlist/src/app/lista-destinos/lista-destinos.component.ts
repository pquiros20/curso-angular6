import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { DestinosApiClient } from '../models/destinos-api-client.models';
import { DestinoViaje } from '../models/destino-viaje.models';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {

    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(d => {
        if (d != null) {
          this.updates.push("se a elejido a " + d.nombre);
        }
      });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);

  }

  ngOnInit() {
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    //this.store.dispatch(new NuevoDestinoAction(d))
  }

  elegido(x: DestinoViaje) {
    this.destinosApiClient.elegir(x);
    // this.store.dispatch(new ElegidoFavoritoAction(x));
  }

  getAll() {

  }


}
